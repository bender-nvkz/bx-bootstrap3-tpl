<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
global $USER;
$APPLICATION->SetPageProperty("description", "bootstrap3 bitrix template");
$APPLICATION->SetPageProperty("author", "Rekuz");
$APPLICATION->SetTitle(CSite::GetByID(SITE_ID)->Fetch()['SITE_NAME']);
if(\Bitrix\Main\Loader::includeModule('rekuz.bootstrap3')) Rekuz\Bootstrap3::init();
if(\Bitrix\Main\Loader::includeModule('rekuz.lessphp')) Rekuz\Lessphp::AddTemplate();
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?$APPLICATION->ShowMeta("description",null,false)?>
        <?$APPLICATION->ShowMeta("author",null,false)?>
        <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">

        <title><?$APPLICATION->ShowTitle()?></title>

        <?$APPLICATION->ShowCSS();?>

        <?$APPLICATION->ShowHeadStrings();?>

    </head>

    <body role="document">
        <? if ($USER->IsAdmin()) $APPLICATION->ShowPanel();?>

        <?$APPLICATION->IncludeComponent("bitrix:menu",".default",Array(
                "ROOT_MENU_TYPE" => "top",
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_CACHE_GET_VARS" => ""
            )
        );?>

        <div class="container" role="main">
