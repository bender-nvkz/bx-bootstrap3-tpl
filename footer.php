<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
global $APPLICATION;
global $DB;
?>
        </div> <!-- /container -->

        <footer class="footer">
            <div class="container">
                <p class="text-muted"><?=date($DB->DateFormatToPHP("YYYY"), time());?> &copy; <? echo CSite::GetByID(SITE_ID)->Fetch()['SITE_NAME']; ?></p>
            </div>
        </footer>

        <?$APPLICATION->ShowHeadScripts()?>

    </body>
</html>