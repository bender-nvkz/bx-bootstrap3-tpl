<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)){ ?>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"><? echo GetMessage("MENU_NAV_TOGGLE"); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"
                   href="<? echo SITE_DIR; ?>"><? echo CSite::GetByID(SITE_ID)->Fetch()['NAME']; ?></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                <? $previousLevel = 0; foreach ($arResult as $arItem){
                if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
                    echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
                }
                if ($arItem["IS_PARENT"]) {
                if ($arItem["DEPTH_LEVEL"] == 1) {
                ?>
                    <li class="dropdown <? if ($arItem["SELECTED"]) { ?>active<? } ?> ">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                           href="<?= $arItem["LINK"] ?>"> <?= $arItem["TEXT"] ?> <span class="caret"></span> </a>
                        <ul class="dropdown-menu" role="menu">
                            <? } else { ?>
                            <li<? if ($arItem["SELECTED"]) { ?> class="active"<? } ?>>
                                <a href="<?= $arItem["LINK"] ?>" class="parent"> <?= $arItem["TEXT"] ?> </a>
                                <ul>
                                    <?
                                    }
                                    } else {
                                        if ($arItem["PERMISSION"] > "D") {
                                            if ($arItem["DEPTH_LEVEL"] == 1) {
                                                ?>
                                                <li<? if ($arItem["SELECTED"]) { ?> class="active"<? } ?>>
                                                    <a href="<?= $arItem["LINK"] ?>"> <?= $arItem["TEXT"] ?> </a>
                                                </li>
                                            <? } else { ?>
                                                <li<? if ($arItem["SELECTED"]) { ?> class="active"<? } ?>>
                                                    <a href="<?= $arItem["LINK"] ?>"> <?= $arItem["TEXT"] ?> </a>
                                                </li>
                                            <?
                                            }
                                        } else {
                                            if ($arItem["DEPTH_LEVEL"] == 1) {
                                                ?>
                                                <li class="dropdown-header"> <?= $arItem["TEXT"] ?> </li>
                                            <? } else { ?>
                                                <li class="dropdown-header"> <?= $arItem["TEXT"] ?> </li>
                                            <?
                                            }
                                        }
                                    }
                                    $previousLevel = $arItem["DEPTH_LEVEL"];
                                    }
                                    if ($previousLevel > 1){
                                        echo str_repeat("</ul></li>", ($previousLevel - 1));
                                    } ?>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
<? } ?>